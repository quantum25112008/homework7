import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Выберите число от 0 до 9:");
        int number = scanner.nextInt();

        switch(number){
            case 0:
                System.out.println("привет");
                break;
            case 1:
                System.out.println("Как дела ?");
            case 2:
                System.out.println("Как прошёл день ?");
                break;
            case 3:
                System.out.println("Как здоровье ?");
                break;
            case 4:
                System.out.println("Как родственники ?");
                break;
            case 5:
                System.out.println("Как семья ?");
                break;
            case 6:
                System.out.println("Знаете как решить задачу ?");
                break;
            case 7:
                System.out.println("Знаете как решить пример ?");
                break;
            case 8:
                System.out.println("Пока");
                break;
            case 9:
                System.out.println("Здарова");
                break;
            default:
                System.out.println("Вы выбрали число не из таблицы");
        }
    }
}